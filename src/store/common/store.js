import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import dsaReducer from "./../dsa/L-recuder";
import wlhReducer from "./../wlh/L-recuder";
import wypReducer from "./../wyp/L-recuder";
import lbhReducer from "./../lbh/L-recuder";
import yycReducer from "./../yyc/L-recuder";
import lmReducer from "./../lm/L-recuder";
import ycyReducer from "./../ycy/L-recuder";
const reducer = combineReducers({
  wlh: wlhReducer,
  lbh: lbhReducer,
  dsa: dsaReducer,
  lm: lmReducer,
  wyp: wypReducer,
  yyc: yycReducer,
  ycy: ycyReducer,
});
const store = createStore(reducer, applyMiddleware(thunk));
export default store;
