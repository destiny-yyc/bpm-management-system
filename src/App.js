import "./App.css";
import { useRoutes, Outlet } from "react-router-dom";
import router from "./router/router";
function App() {
  return (
    <>
      <Outlet />
      {useRoutes(router)}
    </>
  );
}

export default App;
