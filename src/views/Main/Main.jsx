import React from 'react'
import { Layout, Row, Col, Dropdown, Menu, Typography, Space, Alert, Avatar, Image } from 'antd'
import { TextLoop } from 'react-text-loop-next';
import Marquee from 'react-fast-marquee';
import main from './Main.module.css'
import { DownOutlined, BellOutlined, ArrowsAltOutlined, FundFilled, BellFilled } from '@ant-design/icons';
import IconFont from './../../component/IconFont'
import { Outlet } from 'react-router-dom'
import Menusider from './Menu';
import Breafcrumb from './../Breadcrumb/Breafcrumb';
const { Sider, Header, Content, Footer } = Layout;
const { Text, Title } = Typography
export default function Main() {
  const menu = (
    <Menu>
      <Menu.Item>
        超级公司
      </Menu.Item>
    </Menu>
  );
  const menus = (
    <Menu>
      <Menu.Item>
        账号设置
      </Menu.Item>
      <Menu.Item>
        退出
      </Menu.Item>
    </Menu>
  );
  return (
    <>
      <Layout style={{ height: "100vh", userSelect: "none" }}>
        <Sider theme='light' style={{ overflow: "hidden" }} >
          <div className={main.siderText}>BPM管理系统</div>
          <div className={main.userBox} style={{ borderRight: "1px solid #f1f0f0" }}>
            <div className={main.user}>
              <div>
                <Avatar size={40} src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic%2Fb9%2F3c%2Ffa%2Fb93cfa52314bbe29477ce1c02767a477.jpg&refer=http%3A%2F%2Fup.enterdesk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1651068025&t=775515eb84770654c471d19849333c50" />
              </div>
              <div className={main.userText}>
                <Text style={{ color: "#B5B5B5" }}>Alex Yang</Text>
                <Text style={{ color: "#333333", fontSize: "14px" }}>财务总监</Text>
              </div>
            </div>
          </div>
          <div style={{ overflowY: "auto", overflowX: "hidden", height: "calc(100vh - 139px)", borderRight: "1px solid #f1f0f0" }} className='scroll'>
            <Menusider />
          </div>
        </Sider>
        <Layout>
          <Header className={main.header}>
            {/* 公司选择 */}
            <Row>
              <Col span={5}>
                <Dropdown overlay={menu} placement="bottom" trigger={["click"]} className={main.cursor}>
                  <Space>
                    <Text className={main.text}>
                      中国建筑第八工程局有限公司总承包公司
                    </Text>
                    <DownOutlined className={main.text} />
                  </Space>
                </Dropdown>
              </Col>
              {/* 消息提示 */}
              <Col span={17}>
                <Alert className={main.alter} icon={<IconFont type='icon-laba' style={{ color: "white" }} />}
                  banner
                  message={
                    <TextLoop mask className={main.textAlter}>
                      <div>今晚10-12点系统统一升级，敬请悉知！</div>
                      <div>所有员工涨薪一万</div>
                      <div>工作五年的老员工，分10%的股份</div>
                      <div>老板跟小姨子跑了，公司大家分</div>
                    </TextLoop>
                  }
                />
              </Col>
              {/* 右边功能 */}
              <Col span={2}>
                <Space size={"large"}>
                  <FundFilled style={{ fontSize: "20px", color: "white" }} className={main.cursor} />
                  <BellFilled style={{ fontSize: "20px", color: "white" }} className={main.cursor} />
                  <Dropdown overlay={menus} trigger={["click"]} className={main.cursor}>
                    <Avatar size={32} src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic%2Fb9%2F3c%2Ffa%2Fb93cfa52314bbe29477ce1c02767a477.jpg&refer=http%3A%2F%2Fup.enterdesk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1651068025&t=775515eb84770654c471d19849333c50" />
                  </Dropdown>
                </Space>
              </Col>
            </Row>
          </Header>
          <Content className={main.Content}>
            <Row className={main.mianban}>
              <Col span={20} offset={1} className={main.mianban}>
                <Breafcrumb />
              </Col>
            </Row>
            <Row>
              <Col span={22} offset={1} style={{ overflow: "auto", height: "calc(100vh - 48px - 64px)" }}>
                <Outlet />
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
    </>
  )
}
