import React from 'react'
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import IconFont from './../../component/IconFont'
const { SubMenu } = Menu;


export default function Menusider() {
  let handleClick = e => {
    console.log('click ', e);
  };
  const listdata = [
    { title: "首页", icon: <IconFont type='icon-shouye' /> },
    { title: "任务管理", icon: <IconFont type='icon-renwu' />, childred: ["我负责的任务", "我参与的任务", "我创建的任务", "已归档任务"] },
    { title: "项目管理", icon: <IconFont type='icon-renwu' />, childred: ["投标管理", "项目管理"] },
    { title: "合同管理", icon: <IconFont type='icon-renwu' />, childred: ["材料采购合同", "施工班组合同", "项目合同", "常规合同"] },
    { title: "审批管理", icon: <IconFont type='icon-renwu' />, childred: ["我发起的审批", "我经办的审批", "抄送给我的审批"] },
    { title: "招采管理", icon: <IconFont type='icon-renwu' />, childred: ["供应商管理", "施工班组管理", "采购计划管理"] },
    { title: "人事行政管理", icon: <IconFont type='icon-renwu' />, childred: ["通知公告", "考勤统计", "招聘市场"] },
    { title: "财务管理", icon: <IconFont type='icon-renwu' />, childred: ["台账管理", "付款记录", "收款记录"] },
    { title: "知识库管理", icon: <IconFont type='icon-renwu' />, childred: ["知识库列表", "分类管理"] },
    { title: "组织管理", icon: <IconFont type='icon-renwu' />, childred: ["部门管理", "岗位管理", "员工管理"] },
    { title: "设置", icon: <IconFont type='icon-renwu' />, childred: ["审批流设置", "任务模板设置", "考勤组设置", "账号设置"] },

  ]
  return (
    <Menu
      onClick={handleClick}
      defaultSelectedKeys={['1']}
      defaultOpenKeys={['1-1']}
      mode="inline"
      style={{ border: "none", marginTop: "20px" }}
    >
      {listdata.map((item, index) => {
        if (item.childred) {
          return <SubMenu key={index + 1} icon={item.icon} title={item.title}>
            {item.childred.map((el, i) => {
              return <Menu.Item key={`${index + 1}-${i + 1}`}>
                {el}
              </Menu.Item>
            })}
          </SubMenu>
        } else {
          return <Menu.Item key={index + 1} icon={item.icon}>
            {item.title}
          </Menu.Item>
        }
      })}
    </Menu>
  )
}
