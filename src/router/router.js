import Login from "./../views/Login/Login";
import NotFound from "./../views/pag404/NotFound";
import Main from "./../views/Main/Main";
import routerDsa from "./router-dsa";
import routerWlh from "./router-wlh";
import routerLbh from "./router-lbh";
import routerLm from "./router-lm";
import routerWyp from "./router-wyp";
import routerYyc from "./router-yyc";
import routerYcy from "./router-ycy";
export default [
  { path: "/", element: <Login />, index: true },
  {
    path: "/main",
    element: <Main />,
    children: [
      routerDsa,
      routerWlh,
      routerLbh,
      routerLm,
      routerWyp,
      routerYyc,
      routerYcy,
    ],
  },
  { path: "*", element: <NotFound /> },
];
